import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageEmptyComponent } from './page-empty.component';



@NgModule({
  declarations: [PageEmptyComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    PageEmptyComponent
  ]
})
export class PageEmptyModule { }
