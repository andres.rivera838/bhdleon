import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-empty',
  templateUrl: './page-empty.component.html',
  styleUrls: ['./page-empty.component.scss'],
})
export class PageEmptyComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
