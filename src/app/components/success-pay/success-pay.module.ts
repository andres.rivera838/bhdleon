import { IonicModule } from '@ionic/angular';
import { SuccessPayComponent } from './success-pay.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [SuccessPayComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    SuccessPayComponent
  ]
})
export class SuccessPayModule { }
