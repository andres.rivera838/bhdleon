import { TokenService } from './../token/token.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private tokenService: TokenService) { }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    console.log("🚀 ~ file: auth.guard.ts ~ line 16 ~ AuthGuard ~ state", state)
      const url: string = state.url;
      return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    console.log("🚀 ~ file: auth.guard.ts ~ line 21 ~ AuthGuard ~ checkLogin ~ url", url)
    const validSession = this.tokenService.getToken();
    if (url === '/login') {
      if (validSession) {
        this.router.navigateByUrl('/tabs/products');
      }
      return true;
    }

    if (validSession) {
      return true;
    }

    this.router.navigateByUrl('/login');
    return false;
  }

}
