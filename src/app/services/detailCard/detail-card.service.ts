import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DetailCardService {

  private card: any;

  constructor() { }

  setCard(card){
    this.card = card;
  }

  getCard(){
    return this.card;
  }
}
