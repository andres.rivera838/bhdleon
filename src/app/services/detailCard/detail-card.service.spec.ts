import { TestBed } from '@angular/core/testing';

import { DetailCardService } from './detail-card.service';

describe('DetailCardService', () => {
  let service: DetailCardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
