import { TestBed } from '@angular/core/testing';

import { ConsumApiService } from './consum-api.service';

describe('ConsumApiService', () => {
  let service: ConsumApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsumApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
