import { TokenService } from './token/token.service';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ConsumApiService {

  readonly BASE_URL = environment.BASE_URL;

  constructor(private http: HttpClient, private tokenService : TokenService) { }

  consumPost(body, path, header?: boolean): Observable<any> {
    const URLRequest = `${this.BASE_URL}/${path}`;

    let headers;
    if(!!header)
      headers = {"Authorization":  `Bearer ${this.tokenService.getToken()}`}

    console.log("🚀 ~ file: consum-api.service.ts ~ line 17 ~ ConsumApiService ~ consumPost ~ URLRequest", URLRequest)
    return this.http.post(URLRequest, body, {
      headers: headers
    })
      .pipe(
        map((response) => {
          console.log("🚀 ~ file: consum-api.service.ts ~ line 20 ~ ConsumApiService ~ map ~ response", response)
          return response;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  consumGet(path): Observable<any> {
    const URLRequest = `${this.BASE_URL}/${path}`;
    return this.http.get(URLRequest, {
      headers: {"Authorization":  `Bearer ${this.tokenService.getToken()}`}
    })
      .pipe(
        map((response) => {
          console.log("🚀 ~ file: consum-api.service.ts ~ line 37 ~ ConsumApiService ~ map ~ response", response)
          return response;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }
}
