import { DetailCardService } from './../../services/detailCard/detail-card.service';
import { SuccessPayModule } from './../../components/success-pay/success-pay.module';
import { ConsumApiService } from './../../services/consum-api.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { SuccessPayComponent } from '../../components/success-pay/success-pay.component';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-detail-card',
  templateUrl: './detail-card.page.html',
  styleUrls: ['./detail-card.page.scss'],
})
export class DetailCardPage implements OnInit {

  card = {
    BalanceRD: 5000,
    BalanceUS: 200,
    LimitRD: 100000,
    LimitUS: 1000,
    clientName: "Juan Maria Soto Perez",
    productBrand: "Visa",
    productEndingDate: "03/2021",
    productNumber: "4916975561792563"
  }

  name: string;
  date: string;
  toggle: string = "rd";
  value: { Balance: Number, Limit: number, value?: number } = {
    Balance: this.card.BalanceRD,
    Limit: this.card.LimitRD,
    value: (this.card.BalanceRD / this.card.LimitRD) * 1
  }
  showSpinner: boolean = false;

  constructor(private consumApiService: ConsumApiService,
    private toastController: ToastController,
    public modalController: ModalController,
    private route: ActivatedRoute,
    private detailCardService: DetailCardService,
    private router: Router) {


    console.log("🚀 ~ file: detail-card.page.ts ~ line 45 ~ DetailCardPage ~ this.card", this.card)
  }

  ngOnInit() {
    this.card = this.detailCardService.getCard();
    if(!this.card){
      this.router.navigate(['tabs/products']);
    }

    let clientName = this.card.clientName.split(' ');
    this.name = `${clientName[0]} ${clientName[1]}`

    let productEndingDate = this.card.productEndingDate.split('/');
    this.date = `${productEndingDate[0]}/${productEndingDate[1].substring(2, 4)}`;

    this.changeValue('rd');
  }

  changeValue(type) {
    this.toggle = type;
    if (type === "rd") {
      this.value = {
        Balance: this.card.BalanceRD,
        Limit: this.card.LimitRD,
        value: (this.card.BalanceRD / this.card.LimitRD) * 1
      }
    } else {
      this.value = {
        Balance: this.card.BalanceUS,
        Limit: this.card.LimitUS,
        value: (this.card.BalanceUS / this.card.LimitUS) * 1
      }
    }
  }

  async sendPay() {
    try {
      this.showSpinner = true;
      let body = {
        "productNumber": this.card.productNumber,
        "amount": 1
      }
      await this.consumApiService.consumPost(body, 'cards/payment', true).toPromise();
      this.showSpinner = false;
      alert("success")
      /* this.presentModal(); */
    } catch (error) {
      this.presentToast('Error Tecnico');
      this.showSpinner = false;
    }
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SuccessPayComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
