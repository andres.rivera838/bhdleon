import { NumberCardPipe } from './../../pipe/number-card.pipe';
import { SuccessPayModule } from './../../components/success-pay/success-pay.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailCardPageRoutingModule } from './detail-card-routing.module';

import { DetailCardPage } from './detail-card.page';
import { ToolbarModule } from '../../components/toolbar/toolbar.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToolbarModule,
    DetailCardPageRoutingModule,
    SuccessPayModule
  ],
  declarations: [DetailCardPage, NumberCardPipe]
})
export class DetailCardPageModule {}
