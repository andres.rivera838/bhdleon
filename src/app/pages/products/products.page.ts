import { DetailCardService } from './../../services/detailCard/detail-card.service';
import { ConsumApiService } from './../../services/consum-api.service';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})

export class ProductsPage implements OnInit {

  //se podría implementar una interface
  public cards: Array<any> = [];
  public movements: Array<any> = [];
  constructor(private consumApiService: ConsumApiService,
              private router: Router,
              private detailCardService: DetailCardService) { }

  ngOnInit() {
    this.getProducts();
  }

  async getProducts(){
    try {
      this.cards = await this.consumApiService.consumGet('cards').toPromise();
      await this.getMovements(this.cards[0].productNumber);
    } catch (error) {
      alert(error)
    }
  }

  async getMovements(number: string){
    try {
      this.movements = await this.consumApiService.consumGet(`cards/movements/${number}`).toPromise();
    } catch (error) {
      alert(error)
    }
  }

  goToView(card){
    console.log("🚀 ~ file: products.page.ts ~ line 43 ~ ProductsPage ~ goToView ~ card", card)
    this.detailCardService.setCard(card);
    let navigationExtras: NavigationExtras = {
      state: {
        lastPage: "tabs/products"
      }
    }
    this.router.navigate(['detail'], navigationExtras);
  }

}
