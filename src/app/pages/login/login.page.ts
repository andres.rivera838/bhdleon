import { TokenService } from './../../services/token/token.service';
import { ConsumApiService } from './../../services/consum-api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public form: FormGroup;
  public navigationExtras: NavigationExtras = {
    state: {
      lastPage: "login"
    }
  }

  constructor(private formBuilder: FormBuilder,
              private consumApiService: ConsumApiService,
              private router: Router,
              private tokenService: TokenService,
              private navController : NavController) {
    this.form = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      save: new FormControl(false, [Validators.required])
    });
    console.log("🚀 ~ file: login.page.ts ~ line 21 ~ LoginPage ~ constructor ~ this.form", this.form)
   }

  ngOnInit() {
    console.log("🚀 ~ file: login.page.ts ~ line 32 ~ LoginPage ~ ngOnInit ~ this.tokenService.getToken()", this.tokenService.getToken())
    if(!!this.tokenService.getToken()){

      this.router.navigate(['tabs/products'], this.navigationExtras);
    }
  }

  async onSubmit(){
    try {
      if(!!this.form.valid) {
        delete this.form.value.save
        let response = await this.consumApiService.consumPost(this.form.value, 'login', false).toPromise();
        console.log("🚀 ~ file: login.page.ts ~ line 30 ~ LoginPage ~ onSubmit ~ response", response)
        if(!!response.token){
          localStorage.setItem('token', response.token);
          this.router.navigate(['tabs/products'], this.navigationExtras);
        }else{

        }
      }
    } catch (error) {
      alert(error);
    }

  }

  get errorControl() {
    return this.form.controls;
  }


}
