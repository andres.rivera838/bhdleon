import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberCard'
})
export class NumberCardPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    console.log("🚀 ~ file: number-card.pipe.ts ~ line 9 ~ NumberCardPipe ~ transform ~ value", value)

    return this.agregarCaracter(value, " ", 4)
  }

  agregarCaracter(cadena, caracter, pasos) {
    let cadenaConCaracteres = "";
    const longitudCadena = cadena.length;
    for (let i = 0; i < longitudCadena; i += pasos) {
        if (i + pasos < longitudCadena) {
            cadenaConCaracteres += cadena.substring(i, i + pasos) + caracter;
        } else {
            cadenaConCaracteres += cadena.substring(i, longitudCadena);
        }
    }
    return cadenaConCaracteres;
}

}
